#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <SocketIoClient.h>

#define USE_SERIAL Serial

ESP8266WiFiMulti WiFiMulti;
SocketIoClient webSocket;

void event(const char * payload, size_t length) {
  USE_SERIAL.printf("got message: %s\n", payload);
}

void setup() {
    USE_SERIAL.begin(115200);

    USE_SERIAL.setDebugOutput(true);

    USE_SERIAL.println();
    USE_SERIAL.println();
    USE_SERIAL.println();

      for(uint8_t t = 4; t > 0; t--) {
          USE_SERIAL.printf("[SETUP] BOOT WAIT %d...\n", t);
          USE_SERIAL.flush();
          delay(1000);
      }

    WiFiMulti.addAP("CAN_2_plus", "Axcsac12");

    while(WiFiMulti.run() != WL_CONNECTED) {
        delay(100);
        Serial.print('.');
    }

    webSocket.on("event", event);
    webSocket.begin("remote-health-biometric.herokuapp.com", 80);
    // use HTTP Basic Authorization this is optional remove if not needed
    //webSocket.setAuthorization("username", "password");
}
int i =0;
char buffer[255];
void loop() {
    webSocket.loop();
    sprintf(buffer, "{\"device_uuid\": \"3c8f06ad-75df-4d734-84d8-898938eb4aa4\", \"payload\": {\"Red\": 100, \"IR\": 100, \"BPM\": 98, \"spO2\": %d}}", i++);
    webSocket.emit("push data", buffer);  
    
    delay(2000);
}

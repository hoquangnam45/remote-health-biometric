#include <SocketIoClient.h>
#include <ESP8266WiFi.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
//#include <Adafruit_GFX.h>
//#include <Adafruit_ST7735.h>
#include <TFT_eSPI.h> // Hardware-specific library  
#include <Streaming.h>
#include <SPI.h>
#include <Vector.h>
//#include <Arduino.h>
#include <Wire.h>
#include "MAX30105.h"
#include "heartRate.h"
#include <math.h>
#include <ArduinoJson.h>
#include <user_interface.h>
#include "heartRateESP8266Define.h"

TFT_eSPI tft = TFT_eSPI();

SocketIoClient client;


//unsigned long previousMillis = 0;
//int32_t red_raw = 0;
//int32_t red_moving_average = 0;
//int32_t red_unfiltered = 0;
//int32_t red_filtered = 0;
//int32_t ir_raw = 0;
//int32_t ir_moving_average = 0;
//int32_t ir_unfiltered = 0;
//int32_t ir_filtered = 0;
//uint8_t beatDetected = 0;
//uint8_t beat_per_minute = 0;
//uint8_t spo2 = 0;
//uint32_t timestamp = 0;

char refServerTime[100];

int preY = 0;
int i = 0;
int arrANew[MAX_DATA_GRAPH];
Vector<int> vecParamANew (arrANew);
int arrAOld[MAX_DATA_GRAPH];
Vector<int> vecParamAOld (arrAOld);
int arrBNew[MAX_DATA_GRAPH];
Vector<int> vecParamBNew (arrBNew);
int arrBOld[MAX_DATA_GRAPH];
Vector<int> vecParamBOld (arrBOld);

//char character;
//int currentTime = 0;
//boolean isClear = false;

// static char debug_buf[255];
//static uint32_t led_vector[2];

uint32_t timestamp = 0;
uint32_t refCpuTime = 0;
uint32_t old_heartbeat_timestamp;
uint8_t beat_per_minute;
float bpm_temp;
uint8_t beatDetected = 0;
float ratio_average = 0;
uint8_t spo2 = 0;
boolean isDaytime = false;
float bpm_moving_average = 0;
float spo2_moving_average = 0;

void drawGraph();
void redrawReceivedInfo(int32_t ir_raw, int32_t red_raw, uint8_t bpm, uint8_t spo2);
void redrawDeviceUuid();
void getServerRefTime(const char * payload, size_t length);
const char* doCommandBT();

MAX30105 particleSensor;
signal_state red_led, ir_led;

bool emitServerFlag = false;

char commandJSONBuf[MAX_COMMAND_JSON_BUF_SIZE] = {0};//"{\"msg\": 4}";
//char sendJSONBuf[MAX_BUF_SIZE] = {0};
//char payloadServer[MAX_SERVER_JSON_SIZE] = {0};
char payloadServer[MAX_SERVER_PAYLOAD_SIZE] = {0};
char payloadBT[MAX_BLUETOOTH_PAYLOAD_SIZE] = {0};

void setup() {
  debugSerial.begin(115200);
  debugSerial.println("MAX30105 Basic Readings Example");
  BTSerial.begin(115200);
  //  ESP.wdtEnable(1);

  /* Init tft display */
  //Use this initializer if you're using a 1.8" TFT
  tft.init();   //Initialize a ST7735S chip, black tab

  tft.fillScreen(ST7735_BLACK);   //Fill screen with black
  tft.setRotation(0);
  tft.setTextWrap(false);
  tft.drawRect(0, 115, 30, 20, ST7735_CYAN);  // Draw rectangle (x,y,width,height,color)
  tft.drawRoundRect(38, 115, 90, 20, 10, ST7735_CYAN);
  tft.setCursor(5, 122);
  tft.setTextColor(ST7735_WHITE);   //Set color of text. First is the color of text and after is color of background
  tft.setTextSize(1);
  tft.println("BPM");
  tft.drawRect(0, 140, 30, 20, ST7735_CYAN);
  tft.drawRoundRect(38, 140, 90, 20, 10, ST7735_CYAN);
  tft.setCursor(3, 147);
  tft.setTextColor(ST7735_WHITE);
  tft.setTextSize(1);
  tft.println("SPO2");

  for (int k = 0; k < MAX_DATA_GRAPH; k++) {
    vecParamANew.push_back(0);
    vecParamAOld.push_back(0);
    vecParamBNew.push_back(0);
    vecParamBOld.push_back(0);
  }
  tft.fillRect(0, 0, 128, 20, ST7735_BLACK);

  // Initialize sensor
  if (particleSensor.begin() == false) {
    debugSerial.println("MAX30105 was not found. Please check wiring/power. ");
    while (1); // This will cause ESP8266 to watchdog reset
  }

  init_signal_state(&red_led);
  init_signal_state(&ir_led);

  particleSensor.setup(ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange); //Configure sensor with these settings

  /* Wait for bluetooth app to setup emit*/
  if (ENABLE_SETUP_BT_INIT) {
    while (true) {
      const char* doneCommand = doCommandBT();
      //    debugSerial.println("DONE COMMAND: " + doneCommand);
      if (!doneCommand) continue;
      if (!strcmp(doneCommand, "Enable emit") || !strcmp(doneCommand, "Disable emit")) break;
      ESP.wdtFeed();
    }
  }
//  WiFi.begin("CAN_2_plus", "Axcsac12");
  while(emitServerFlag){
    if (wifi_station_get_connect_status() == STATION_GOT_IP) {
      client.begin(HANDLED_SERVER_IP, HANDLED_SERVER_PORT, "/socket.io/?EIO=3&transport=websocket");
      //client.begin("192.168.43.157", 3000);
      client.emit("request time");
     
      client.on("return time", getServerRefTime);
      while(!isDaytime) {
        client.loop();
        yield();
      }
      isDaytime = false;
      // Send to server  and bluetooth
      refCpuTime = millis();
      tft.setCursor(0, 0);
      tft.setTextColor(ST7735_WHITE);   
      tft.setTextSize(1);
      tft.println(refServerTime);
      delay(1000);
      debugSerial.printf("got message: %s\n", refServerTime);
      tft.fillRect(0, 0, 128, 20, ST7735_BLACK);
      break;
    }
    doCommandBT();
  }


  //    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  //    debugSerial.print("Ket noi vao mang ");
  //    tft.setCursor(0, 0);
  //
  //    while (WiFi.status() != WL_CONNECTED) {
  //      delay(500);
  //      tft.setTextColor(ST7735_WHITE);
  //      tft.setTextSize(1);
  //      tft.print('.');
  //      debugSerial.print('.');
  //      delay(1000);
  //    }
  //    tft.fillRect(0, 0, 128, 20, ST7735_BLACK);
  //    tft.setCursor(0, 0);
  //    tft.setTextColor(ST7735_WHITE);
  //    tft.setTextSize(1);
  //    tft.println(F("Da ket noi WiFi"));
  //    debugSerial.println(F("Da ket noi WiFi"));
  //    debugSerial.println(F("Dia chi IP cua ESP8266 (Socket Client ESP8266): "));
  //    debugSerial.println(WiFi.localIP());
  //    delay(1000);
  //    client.begin(HANDLED_SERVER_IP, HANDLED_SERVER_PORT);
  //    //client.begin("192.168.43.157", 3000);
  //    client.emit("request time");
  //
  //    client.on("return time", event);
  //    while(!isDaytime) {
  //      client.loop();
  //      yield();
  //    }
  //    tft.setCursor(0, 0);
  //    tft.setTextColor(ST7735_WHITE);
  //    tft.setTextSize(1);
  //    tft.println(refServerTime);
  //    delay(1000);
  //    debugSerial.printf("got message: %s\n", refServerTime);
  //    tft.fillRect(0, 0, 128, 20, ST7735_BLACK);
  //  }
}


void loop() {
  static int max_graph_A = MAX_DATA_GRAPH - 1;
  static int max_graph_B = MAX_DATA_GRAPH - 1;
  static int countPayload = 0;
  //  client.emit("ping");
  //  client.loop();
  //  static int btTime = 0;

  doCommandBT();

  if (!calcHeartRateSpO2Routine()) return;

  //  long startMillis = millis();
  drawGraph();

  uint8_t tft_bpm = (uint8_t) floor(bpm_moving_average);
  uint8_t tft_spo2 = (uint8_t) floor(spo2_moving_average);
  redrawReceivedInfo(ir_led.fir_filtered_signal, red_led.fir_filtered_signal, tft_bpm, tft_spo2);

  redrawDeviceUuid();

  sprintf(payloadBT, "%u;%u;%u\n", 
//          red_led.fir_filtered_signal,
//          ir_led.fir_filtered_signal,
          (uint8_t) floor(spo2_moving_average),
          (uint8_t) floor(bpm_moving_average),
          timestamp);
  //  sprintf(payloadBT, "{\"device_uuid\":\"%s\","
  //                        "\"payload\": [{"
  //                          "\"Red\":%d,"
  //                          "\"IR\":%d,"
  //                          "\"BPM\":%u,"
  //                          "\"spO2\":%u,"
  ////                          "\"originTimestamp\":\"%s\","
  //                          "\"cpuTimestamp\":%u"
  //                         "}]}\n",
  //          DEVICE_UUID,
  //          red_led.fir_filtered_signal,
  //          ir_led.fir_filtered_signal,
  //          (uint8_t) floor(bpm_moving_average),
  //          (uint8_t) floor(spo2_moving_average),
  ////          refServerTime,
  //          timestamp - refCpuTime);

  /*******************************/
  if (emitServerFlag) {
//    if (!countPayload) sprintf(payloadServer, 
//                                "{\"device_uuid\":\"%s\","
//                                "\"refServerTime\":\"%s\","
//                                "\"refCpuTime\":%u,"
//                                "\"payload\":[", 
//                                DEVICE_UUID, 
//                                refServerTime,
//                                refCpuTime);
    if (!countPayload) sprintf(payloadServer, 
                                "\""
                                "["
                                  "["
                                    "\\\"%s\\\","
                                    "\\\"%s\\\","
                                    "%u"
                                  "]"
                                ","
                                , 
                                DEVICE_UUID, 
                                refServerTime,
                                refCpuTime);
    
//    sprintf(payloadServer + strlen(payloadServer), 
//            "{"
//              "\"Red\":%d,"
//              "\"IR\":%d,"
//              "\"RedFil\":%d,"
//              "\"IRFil\":%d,"
//              "\"RedMA\":%d,"
//              "\"IRMA\":%d,"
//              "\"BPM\":%u,"
//              "\"spO2\":%u,"
//              "\"isBeat\":%u,"
//              "\"millis\":%u"
//            "}"
//            ,
//            red_led.raw_signal,
//            ir_led.raw_signal,
//            red_led.fir_filtered_signal,
//            ir_led.fir_filtered_signal,
//            red_led.moving_average_estimate,
//            ir_led.moving_average_estimate,
//            (uint8_t) floor(bpm_moving_average),
//            (uint8_t) floor(spo2_moving_average),
//            beatDetected,
//            timestamp);
    sprintf(payloadServer + strlen(payloadServer), 
            "["
              "%d,"
              "%d,"
              "%d,"
              "%d,"
              "%d,"
              "%d,"
              "%u,"
              "%u,"
              "%u,"
              "%u"
            "]"
            ,
            red_led.raw_signal,
            ir_led.raw_signal,
            red_led.fir_filtered_signal,
            ir_led.fir_filtered_signal,
            red_led.moving_average_estimate,
            ir_led.moving_average_estimate,
            (uint8_t) floor(bpm_moving_average),
            (uint8_t) floor(spo2_moving_average),
            beatDetected,
            timestamp);
            
    if (countPayload < NUM_PAYLOAD_SERVER_LIMIT - 1) strcat(payloadServer, ",");
    else {
      strcat(payloadServer, "]\"");
//      debugSerial.println(payloadServer);
      client.emit("push data", payloadServer);
//      client.loop();
    }
    countPayload = (countPayload + 1) % (NUM_PAYLOAD_SERVER_LIMIT);
  }
  /*******************************/

//  if (emitBT){
    //  if (millis() - btTime >= 1000) {
    BTSerial.print(payloadBT);
    //    btTime= millis();
    //  }
//  }
  i = 0;
  max_graph_A = MAX_DATA_GRAPH - 1;
  max_graph_B = MAX_DATA_GRAPH - 1;
  pythonDebugSerial.printf("%d:%d:%d:%d:%d:%d:%d:%d:%u:%u:%u:%u\r\n", 
                           red_led.raw_signal, \
                           red_led.moving_average_estimate, \
                           red_led.fir_unfiltered_signal, \
                           red_led.fir_filtered_signal, \
                           ir_led.raw_signal, \
                           ir_led.moving_average_estimate, \
                           ir_led.fir_unfiltered_signal, \
                           ir_led.fir_filtered_signal, \
                           beatDetected, \
                           beat_per_minute, \
                           spo2, \
                           timestamp);
  client.loop();
  //  long endMillis = millis();
  //  BTSerial.println(endMillis - startMillis);
}
//void loop(){
//  client.emit("test debug", "\"haha\"");
//  client.loop();
//  Serial.println("WTF");
//  delay(1000);
//  
//  
//  
//}

const char* doCommandBT() {
  static int bufIdx = 0;
  static int resetCommandBufFlag = 0;

  while (BTSerial.available()) {
    if (resetCommandBufFlag) {
      bufIdx = 0;
      commandJSONBuf[bufIdx] = '\0';
      resetCommandBufFlag = 0;
    }
    char receivedCharacter = BTSerial.read();
    commandJSONBuf[bufIdx++] = receivedCharacter;

    if (receivedCharacter != '\n' && receivedCharacter != '\r') continue;

    // Remove stray '\r' or stray '\n' from serial buffer
    while (BTSerial.peek() == '\n' || BTSerial.peek() == '\r') BTSerial.read();

    commandJSONBuf[bufIdx - 1] = '\0';

    DynamicJsonDocument jsonObj(100);
    DeserializationError err = deserializeJson(jsonObj, commandJSONBuf);

    resetCommandBufFlag = 1;

    if (err) {
      debugSerial.print("deserializeJson() failed: ");
      debugSerial.println(err.c_str());
      debugSerial.println(commandJSONBuf);
      return (err.c_str());
    }

    if (!jsonObj["command"]) break;


    if (!strcmp(jsonObj["command"].as<const char*>(), "Enable emit")) {
      //      if(wifi_station_get_connect_status() != STATION_GOT_IP){
      //        jsonObj["command result"] = "Can't emit to server. Wifi is disconnected";
      //        serializeJson(jsonObj, sendJSONBuf);
      //        BTSerial.println(sendJSONBuf);
      //        debugSerial.println(sendJSONBuf);
      //        sendJSONBuf[0] = '\0';
      //        break;
      //      }
      emitServerFlag = true;
      //      jsonObj["command result"] = "Enable emit successfully";
      sprintf(payloadBT,
              "{\"command\": \"%s\","
              "\"command result\": \"%s\""
              "}\n", jsonObj["command"].as<const char*>(), "Enable emit successfully");
      //      serializeJson(jsonObj, BTSerial);
      //      serializeJson(jsonObj, debugSerial);
      BTSerial.print(payloadBT);
      debugSerial.print(payloadBT);
      //      BTSerial.println(sendJSONBuf);
      //      debugSerial.println(sendJSONBuf);
      //      sendJSONBuf[0] = '\0';
      /* Wait for bluetooth app to setup wifi*/
      
      return jsonObj["command"].as<const char*>();
    }





    else if (!strcmp(jsonObj["command"].as<const char*>(), "Disable emit")) {
      //      WiFi.disconnect();
      emitServerFlag = false;
      //      jsonObj["command result"] = "Disable emit successfully";
      sprintf(payloadBT,
              "{\"command\": \"%s\","
              "\"command result\": \"%s\""
              "}\n", jsonObj["command"].as<const char*>(), "Disable emit successfully");
      //      serializeJson(jsonObj, BTSerial);
      //      serializeJson(jsonObj, debugSerial);
      BTSerial.print(payloadBT);
      debugSerial.print(payloadBT);
      //      BTSerial.println(sendJSONBuf);
      //      debugSerial.println(sendJSONBuf);
      //      sendJSONBuf[0] = '\0';
      return jsonObj["command"].as<const char*>();
    }



    else if (!strcmp(jsonObj["command"].as<const char*>(), "Connect to wifi")) {
      if (!jsonObj["wifi ssid"]) {
        //        jsonObj["command result"] = "No specify wifi ssid";
        sprintf(payloadBT,
                "{\"command\": \"%s\","
                "\"command result\": \"%s\""
                "}\n", jsonObj["command"].as<const char*>(), "No specify wifi ssid");
        //      serializeJson(jsonObj, BTSerial);
        //      serializeJson(jsonObj, debugSerial);
        BTSerial.print(payloadBT);
        debugSerial.print(payloadBT);
        //      BTSerial.println(sendJSONBuf);
        //      debugSerial.println(sendJSONBuf);
        //      sendJSONBuf[0] = '\0';
        break;
      }
      if (jsonObj["wifi pass"])
        WiFi.begin(jsonObj["wifi ssid"].as<const char*>(), jsonObj["wifi pass"].as<const char*>());
      else WiFi.begin(jsonObj["wifi ssid"].as<const char*>());

      debugSerial.print("Ket noi vao mang ");
      tft.setCursor(0, 0);

      char* statusConnectStr = NULL;
      while (wifi_station_get_connect_status() != STATION_GOT_IP) {
        switch (wifi_station_get_connect_status()) {
          case STATION_WRONG_PASSWORD:
            statusConnectStr = "Wrong wifi password. Pls reconnect";
            break;
          case STATION_NO_AP_FOUND:
            statusConnectStr = "No AP Found";
            break;
          case STATION_CONNECT_FAIL:
            statusConnectStr = "Connect fail. Pls reconnect";
            break;
          case STATION_GOT_IP:
          case STATION_IDLE:
          case STATION_CONNECTING:
            break;
          default:
            statusConnectStr = "Something wrong with connection. Pls reset";
            break;
        }
        if (statusConnectStr) break;
        delay(500);
        tft.setTextColor(ST7735_WHITE);
        tft.setTextSize(1);
        tft.print('.');
        debugSerial.print('.');
        delay(1000);
      }
      if (wifi_station_get_connect_status() !=  STATION_GOT_IP) {
        sprintf(payloadBT,
                "{\"command\": \"%s\","
                "\"command result\": \"%s\""
                "}\n", jsonObj["command"].as<const char*>(), statusConnectStr);
        //      serializeJson(jsonObj, BTSerial);
        //      serializeJson(jsonObj, debugSerial);
        BTSerial.print(payloadBT);
        debugSerial.print(payloadBT);
        //      BTSerial.println(sendJSONBuf);
        //      debugSerial.println(sendJSONBuf);
        //      sendJSONBuf[0] = '\0';
        break;
      }
      //      jsonObj["command result"] = "Connect successfully";
      sprintf(payloadBT,
              "{\"command\": \"%s\","
              "\"command result\": \"%s\""
              "}\n", jsonObj["command"].as<const char*>(), "Connect successfully");
      //      serializeJson(jsonObj, BTSerial);
      //      serializeJson(jsonObj, debugSerial);
      BTSerial.print(payloadBT);
      debugSerial.print(payloadBT);
      //      BTSerial.println(sendJSONBuf);
      //      debugSerial.println(sendJSONBuf);
      //      sendJSONBuf[0] = '\0';

      tft.fillRect(0, 0, 128, 20, ST7735_BLACK);
      tft.setCursor(0, 0);
      tft.setTextColor(ST7735_WHITE);
      tft.setTextSize(1);
      tft.println(F("Da ket noi WiFi"));
      debugSerial.println(F("Da ket noi WiFi"));
      debugSerial.println(F("Dia chi IP cua ESP8266 (Socket Client ESP8266): "));
      debugSerial.println(WiFi.localIP());
      return jsonObj["command"].as<const char*>();
    }




    else if (!strcmp(jsonObj["command"].as<const char*>(), "Disconnect wifi")) {
      WiFi.disconnect();
      //      jsonObj["command result"] = "Disconnect wifi successfully";
      sprintf(payloadBT,
              "{\"command\": \"%s\","
              "\"command result\": \"%s\""
              "}\n", jsonObj["command"].as<const char*>(), "Disconnect wifi successfully");
      //      serializeJson(jsonObj, BTSerial);
      //      serializeJson(jsonObj, debugSerial);
      BTSerial.print(payloadBT);
      debugSerial.print(payloadBT);
      //      BTSerial.println(sendJSONBuf);
      //      debugSerial.println(sendJSONBuf);
      //      sendJSONBuf[0] = '\0';
      return jsonObj["command"].as<const char*>();
    }



    else if (!strcmp(jsonObj["command"].as<const char*>(), "Reset device")) {
      //      jsonObj["command result"] = "Begin resetting device";
      sprintf(payloadBT,
              "{\"command\": \"%s\","
              "\"command result\": \"%s\""
              "}\n", jsonObj["command"].as<const char*>(), "Begin resetting device");
      //      serializeJson(jsonObj, BTSerial);
      //      serializeJson(jsonObj, debugSerial);
      BTSerial.print(payloadBT);
      debugSerial.print(payloadBT);
      //      BTSerial.println(sendJSONBuf);
      //      debugSerial.println(sendJSONBuf);
      //      sendJSONBuf[0] = '\0';
      while (1); // This will cause ESP8266 to watchdog reset
    }



    else if (!strcmp(jsonObj["command"].as<const char*>(), "Return connect status")) {
      //      JsonObject resultObj = jsonObj["command result"].createNestedObject();
      char* statusConnectStr = NULL;
      switch (wifi_station_get_connect_status()) {
        case STATION_WRONG_PASSWORD:
          statusConnectStr = "STATION WRONG PASSWORD";
          break;
        case STATION_NO_AP_FOUND:
          statusConnectStr = "STATION NO AP FOUND";
          break;
        case STATION_CONNECT_FAIL:
          statusConnectStr = "STATION CONNECT FAIL";
          break;
        case STATION_GOT_IP:
          statusConnectStr = "STATION GOT IP";
          break;
        case STATION_IDLE:
          statusConnectStr = "STATION IDLE";
          break;
        case STATION_CONNECTING:
          statusConnectStr = "STATION CONNECTING";
          break;
        default:
          statusConnectStr = "UNKNOWN STATE";
          break;
      }
      //        resultObj["wifi ssid"] = WiFi.SSID();
      //        resultObj["wifi ip"] = WiFi.localIP().toString();
      //
      //        resultObj["emit status"] = emitServerFlag;
      //        resultObj["emit addr"] = HANDLED_SERVER_IP;
      //        resultObj["uuid"] = DEVICE_UUID;


      sprintf(payloadBT,
              "{\"command\": \"%s\","
              "\"command result\": {"
              "\"connect state\": \"%s\","
              "\"wifi ssid\": \"%s\","
              "\"wifi ip\": \"%s\","
              "\"emit status\": \"%s\","
              "\"emit addr\": \"%s\","
              "\"uuid\": \"%s\""
              "}"
              "}\n",
              jsonObj["command"].as<const char*>(),
              statusConnectStr,
              WiFi.SSID().c_str(),
              WiFi.localIP().toString().c_str(),
              emitServerFlag ? "true" : "false",
              HANDLED_SERVER_IP,
              DEVICE_UUID);
      //      serializeJson(jsonObj, BTSerial);
      //      serializeJson(jsonObj, debugSerial);
      BTSerial.print(payloadBT);
      debugSerial.print(payloadBT);
      //      BTSerial.println(sendJSONBuf);
      //      debugSerial.println(sendJSONBuf);
      //      sendJSONBuf[0] = '\0';
      return jsonObj["command"].as<const char*>();
    }



    else if (!strcmp(jsonObj["command"].as<const char*>(), "Scan nearby wifi")) {
      debugSerial.print("\nScan start ... ");
      int n = WiFi.scanNetworks();
      debugSerial.print("\nScan done... \n");

      // create nested array
      //      JsonArray wifi_list = jsonObj.createNestedArray("wifi list");
      sprintf(payloadBT,
              "{\"command\": \"%s\","
              "\"command result\": \"%d wifi found\","
              "\"wifi list\": [",
              jsonObj["command"].as<const char*>(),
              n);
      for (int i = 0; i < n; i++) {
        sprintf(payloadBT + strlen(payloadBT),
                "{\"ssid\": \"%s\","
                "\"rssi\": %d,"
                "\"enc\": \"%s\","
                "\"channel\": \"%d\"},",
                WiFi.SSID(i).c_str(),
                WiFi.RSSI(i),
                (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? "no" : "yes",
                WiFi.channel(i)
               );
        //        JsonObject wifi_info = wifi_list.createNestedObject();//(JSON_OBJECT_SIZE(10));
        //        wifi_info["ssid"] = WiFi.SSID(i);
        //        wifi_info["rssi"] = WiFi.RSSI(i);
        //        wifi_info["enc"] = (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? "no" : "yes";
        //debugSerial.printf("%d: %s, Ch:%d (%ddBm) %s\n", i+1, WiFi.SSID(i).c_str(), WiFi.channel(i), WiFi.RSSI(i), WiFi.encryptionType(i) == ENC_TYPE_NONE ? "open" : "");
      }
      WiFi.scanDelete();
      sprintf(payloadBT + strlen(payloadBT) - 1, // -1 to remove last comma
              "]"
              "}\n");
      //      serializeJson(jsonObj, BTSerial);
      //      serializeJson(jsonObj, debugSerial);
      BTSerial.print(payloadBT);
      debugSerial.print(payloadBT);
      //      jsonObj["command result"] = String(n) + " wifi found";
      //      serializeJson(jsonObj, BTSerial);
      //      serializeJson(jsonObj, debugSerial);
      //      BTSerial.println(sendJSONBuf);
      //      debugSerial.println(sendJSONBuf);
      //      sendJSONBuf[0] = '\0';

      return jsonObj["command"].as<const char*>();
    }





    else {
      jsonObj["command result"] = "Invalid command";
      sprintf(payloadBT,
              "{\"command\": \"%s\","
              "\"command result\": \"%s\""
              "}\n", jsonObj["command"].as<const char*>(), "Invalid command");
      //      serializeJson(jsonObj, BTSerial);
      //      serializeJson(jsonObj, debugSerial);
      BTSerial.print(payloadBT);
      debugSerial.print(payloadBT);
      //      BTSerial.println(sendJSONBuf);
      //      debugSerial.println(sendJSONBuf);
      //      sendJSONBuf[0] = '\0';
      break;
    }
  }
  return NULL;
}

bool calcHeartRateSpO2Routine() {

  if (!particleSensor.safeCheck(250)) return false;

  timestamp = millis();
  //  // Send to server  and bluetooth
  //  if (!refCpuTime) refCpuTime = timestamp;

  red_led.raw_signal = (int32_t) particleSensor.getFIFORed();
  ir_led.raw_signal = (int32_t) particleSensor.getFIFOIR();

  processSignal(&red_led);
  processSignal(&ir_led);

  if (timestamp - old_heartbeat_timestamp >= BPM_RESET_TIME) {
    beat_per_minute = 0;
    bpm_moving_average = 0;
  }

  beatDetected = checkForBeat(&ir_led);

  if (beatDetected) {
    //printf("hihi\r\n");
    bpm_temp = 60000. / (timestamp - old_heartbeat_timestamp);
    if (bpm_temp < 180 && bpm_temp > 30) beat_per_minute = (uint8_t) bpm_temp;
    //printf("%f; %d; %d; %d\r\n", bpm_temp, timestamp, old_heartbeat_timestamp, timestamp - old_heartbeat_timestamp);
    old_heartbeat_timestamp = timestamp;
    bpm_moving_average -= bpm_moving_average / BPM_MOVING_AVERAGE_SIZE;
    bpm_moving_average += beat_per_minute / BPM_MOVING_AVERAGE_SIZE;
    //printf("%f\r\n",bpm_temp);
    //if (bpm_temp < 255) beat_per_minute = (uint8_t) bpm_temp;
  }

  if ((ir_led.peak_max - ir_led.valley_min > 0) && (red_led.moving_average_estimate > RED_LED_MOVING_AVERAGE_SPO2_CALC_THRESH)) {
    ratio_average = (float)(red_led.peak_max - red_led.valley_min) / (ir_led.peak_max - ir_led.valley_min) * ir_led.moving_average_estimate / red_led.moving_average_estimate;
    spo2 = floor(110 - 25 / ratio_average);
    if (spo2 > 100) spo2 = 100;
  }
  else {
    spo2 = 0;
  }


  spo2_moving_average -= spo2_moving_average / SPO2_MOVING_AVERAGE_SIZE;
  spo2_moving_average += spo2 / SPO2_MOVING_AVERAGE_SIZE;
  return true;
}


void getServerRefTime(const char * payload, size_t length) {
  sscanf( payload, "%s", refServerTime);
  isDaytime = true;
}

void redrawReceivedInfo(int32_t ir_raw, int32_t red_raw, uint8_t bpm, uint8_t spo2) {
  static int32_t pre_red = 0;
  static int32_t pre_ir = 0;
  static int32_t pre_bpm = 0;
  static int32_t pre_spo2 = 0;

  tft.setCursor(43, 122);
  tft.setTextColor(ST7735_BLACK);  // Set color of text. First is the color of text and after is color of background
  tft.setTextSize(1);  // Set text size. Goes from 0 (the smallest) to 20 (very big)
  tft.print(pre_bpm);

  tft.setCursor(43, 147);
  tft.setTextColor(ST7735_BLACK);  // Set color of text. First is the color of text and after is color of background
  tft.setTextSize(1);  // Set text size. Goes from 0 (the smallest) to 20 (very big)
  tft.print(pre_spo2);

  pre_bpm = bpm;
  pre_spo2 = spo2;
  // It draws from the location to down-right
  tft.setCursor(43, 122);  // Set position (x,y)
  tft.setTextColor(ST7735_WHITE);  // Set color of text. First is the color of text and after is color of background
  tft.setTextSize(1);  // Set text size. Goes from 0 (the smallest) to 20 (very big)
  tft.print(bpm);

  // It draws from the location to down-right
  tft.setCursor(43, 147);  // Set position (x,y)
  tft.setTextColor(ST7735_WHITE);  // Set color of text. First is the color of text and after is color of background
  tft.setTextSize(1);  // Set text size. Goes from 0 (the smallest) to 20 (very big)
  tft.print(spo2);
}

void redrawDeviceUuid() {
  const char* uuidDoubleStr = DEVICE_UUID "" DEVICE_UUID;
  static int countDrawUuidCycle = 0;
  static int textOffset = 0;
  countDrawUuidCycle = (countDrawUuidCycle + 1) % UNIT_40MS_UUID;
  if (countDrawUuidCycle < UNIT_40MS_UUID - 1) return;
  //  Show UUID to tft LCD
  //  char partUuidStrNew[36];
  //  char partUuidStrOld[36];

  //  for (int k = 0; k < WIDTH_UUID; k++)
  //    t += text.charAt((offset + k) % text.length());

  tft.setTextWrap(false); // Don't wrap text to next line
  // Print the string for this iteration
  tft.setCursor(0, 0); // display will be halfway down screen
  tft.setTextColor(ST7735_BLACK);  // Set color of text. First is the color of text and after is color of background
  tft.setTextSize(1);
  tft.print(uuidDoubleStr + textOffset);

  textOffset = ++textOffset % (strlen(uuidDoubleStr) / 2);
  tft.setCursor(0, 0); // display will be halfway down screen
  tft.setTextColor(ST7735_WHITE);  // Set color of text. First is the color of text and after is color of background
  tft.setTextSize(1);
  tft.print(uuidDoubleStr + textOffset);
}

void drawGraph() {
  vecParamANew.remove(0);
  vecParamBNew.remove(0);
  if (ir_led.fir_filtered_signal + SCALE_SIGNED_VALUE <= MOD_VALUE_SCREEN_PIXEL * HEIGHT_COORDINATE_AXIS && ir_led.fir_filtered_signal + SCALE_SIGNED_VALUE >= 0) {
    vecParamANew.push_back((ir_led.fir_filtered_signal + SCALE_SIGNED_VALUE) / MOD_VALUE_SCREEN_PIXEL);
  } else if (ir_led.fir_filtered_signal + SCALE_SIGNED_VALUE > MOD_VALUE_SCREEN_PIXEL * HEIGHT_COORDINATE_AXIS) {
    vecParamANew.push_back(HEIGHT_COORDINATE_AXIS);
  } else {
    vecParamANew.push_back(0);
  }

  if (red_led.fir_filtered_signal + SCALE_SIGNED_VALUE <= MOD_VALUE_SCREEN_PIXEL * HEIGHT_COORDINATE_AXIS && red_led.fir_filtered_signal + SCALE_SIGNED_VALUE >= 0) {
    vecParamBNew.push_back((red_led.fir_filtered_signal + SCALE_SIGNED_VALUE ) / MOD_VALUE_SCREEN_PIXEL);
  } else if (red_led.fir_filtered_signal + SCALE_SIGNED_VALUE > MOD_VALUE_SCREEN_PIXEL * HEIGHT_COORDINATE_AXIS) {
    vecParamBNew.push_back(HEIGHT_COORDINATE_AXIS);
  } else {
    vecParamBNew.push_back(0);
  }

  tft.drawLine(0, ORGIN_RAW - 50, 128, ORGIN_RAW - 50, ST7735_YELLOW);
  for (int coordinate = MAX_DATA_GRAPH - 1; coordinate > 0; coordinate--) {
    tft.drawLine(coordinate * TIME_UNIT, ORGIN_RAW - vecParamBOld[coordinate], (coordinate - 1)*TIME_UNIT, ORGIN_RAW - vecParamBOld[coordinate - 1], ST7735_BLACK);
    vecParamBOld[coordinate] = vecParamBNew[coordinate];
    tft.drawLine(coordinate * TIME_UNIT, ORGIN_RAW - vecParamBNew[coordinate], (coordinate - 1)*TIME_UNIT, ORGIN_RAW - vecParamBNew[coordinate - 1], ST7735_RED);
    tft.drawLine(coordinate * TIME_UNIT, ORGIN_RAW - vecParamAOld[coordinate], (coordinate - 1)*TIME_UNIT, ORGIN_RAW - vecParamAOld[coordinate - 1], ST7735_BLACK);
    vecParamAOld[coordinate] = vecParamANew[coordinate];
    tft.drawLine(coordinate * TIME_UNIT, ORGIN_RAW - vecParamANew[coordinate], (coordinate - 1)*TIME_UNIT, ORGIN_RAW - vecParamANew[coordinate - 1], ST7735_GREEN);
  }
  vecParamBOld[0] = vecParamBNew[0];
  vecParamAOld[0] = vecParamANew[0];
}

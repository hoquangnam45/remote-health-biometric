#define ledBrightness 0x3F
#define sampleAverage 16
#define ledMode 2
#define sampleRate 400
#define pulseWidth 411
#define adcRange 4096
#define BPM_RESET_TIME 2000

#define RED_LED_MOVING_AVERAGE_SPO2_CALC_THRESH 160000

class NullSerial : public Stream {
public:
  virtual size_t write(uint8_t) { return (1); }
  virtual int available() { return (0); }
  virtual int read() { return (0); }
  virtual int peek() { return (0); }
  virtual void flush() {}
  void begin(unsigned long) {}
  void end() {}
} NullSerial_;

//#define DEBUG_PRINT

#ifdef DEBUG_PRINT
  #include <SoftwareSerial.h>
  #define SOFTWARESERIAL_RX D8
  #define SOFTWARESERIAL_TX D0 
  SoftwareSerial SoftSerial(SOFTWARESERIAL_RX, SOFTWARESERIAL_TX);
  #define debugSerial Serial
  #define BTSerial SoftSerial
  #define pythonDebugSerial Serial

#else
  #define debugSerial NullSerial_
  #define BTSerial Serial
  #define pythonDebugSerial NullSerial_
#endif

// Check User_Setup.h
//  //SCK (CLK) ---> NodeMCU pin D5 (GPIO14)
//  //MOSI(DIN) ---> NodeMCU pin D7 (GPIO13)
//  #define TFT_RST        
//  #define TFT_CS    
//  #define TFT_DC    

#define DEVICE_UUID "3c8f06ad-75df-4734-84d8-898938eb4ba4"
#define UNIT_40MS_UUID 3

#define SCALE_SIGNED_VALUE 2500
#define HEIGHT_COORDINATE_AXIS  100
#define MOD_VALUE_SCREEN_PIXEL (SCALE_SIGNED_VALUE*2/HEIGHT_COORDINATE_AXIS)
#define TIME_UNIT 2
#define ORGIN_RAW 110
#define MAX_DATA_GRAPH 128/TIME_UNIT + 1

#define ENABLE_SETUP_BT_INIT true

#define HANDLED_SERVER_IP "remote-health-biometric.herokuapp.com"
#define HANDLED_SERVER_PORT 80
//#define HANDLED_SERVER_IP "192.168.1.140"
//#define HANDLED_SERVER_PORT 3000
#define NUM_PAYLOAD_SERVER_LIMIT 25

#define MAX_COMMAND_JSON_BUF_SIZE 100
#define MAX_SERVER_PAYLOAD_SIZE 2000
#define MAX_BLUETOOTH_PAYLOAD_SIZE 1024
#define BPM_MOVING_AVERAGE_SIZE 5
#define SPO2_MOVING_AVERAGE_SIZE 5
